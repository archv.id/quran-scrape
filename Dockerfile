FROM node:12.14.0-alpine

MAINTAINER Risky Makira <risky@makira.id>

WORKDIR /usr/app

COPY package.json .

RUN npm install --quiet

COPY . .