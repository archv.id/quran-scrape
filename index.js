const fetch = require('node-fetch');
const fs = require('fs');
const delay = require('delay');

const getListSurah = () => new Promise((resolve, reject) => {
    fetch('https://quran.kemenag.go.id/index.php/api/v1/surat', {
        method:'GET'
    })
    .then(res => res.json())
    .then(res => {
        resolve(res)
    })
    .catch(err => {
        reject(err)
    })
});

const getSuratAyat = (surahId) => new Promise((resolve, reject) => {
    fetch(`https://quran.kemenag.go.id/index.php/api/v1/ayatweb/${surahId}/0/0/1000000`, {
        method:'GET'
    })
    .then(res => res.json())
    .then(res => {
        resolve(res)
    })
    .catch(err => {
        reject(err)
    })
});

(async () => {
    const surah = await getListSurah();
    fs.writeFileSync('./data/surah.json', JSON.stringify(surah.data, 0, 2))
    const ayats = [];
    for (let index = 0; index < surah.data.length; index++) {
        const datas = surah.data[index];
        await delay(5000)
        console.log('mengambil data surat : ', datas.surat_name)
        const ayat = await getSuratAyat(datas.id);
        ayat.data.map(datas => {
            ayats.push(datas);
        })
    }

    console.log(ayats.length)
    fs.writeFileSync('./data/ayat.json', JSON.stringify(ayats, 0, 2))

})();